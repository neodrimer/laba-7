public class task4 {
    public static int sumConsecutive(int[] array, int s, int m)
    {
        int sum = 0;
        for (int i = s; i <= m; i++)
        {
            sum += array[i];
        }
        return sum;
    }

    public static void main(String[] args)
    {
        int[] array = {1, 2, 1, 2, 1, 2};
        int lenghtArray = array.length;
        int s = 0;
        int m = 2;
        int sum = 0;

        while (m<lenghtArray)
        {
            sum = sumConsecutive(array, s, m);
            System.out.println(sum);
            s++;
            m++;
        }
    }
}
