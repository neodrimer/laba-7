public class task3 {
    public static boolean isArmstrongNumber(int n)
    {
        int num = n;
        int digits = 0;
        int sum = 0;
        while (num > 0)
        {
            digits++;
            num /= 10;
        }
        num = n;
        while (num > 0)
        {
            int d = num % 10;
            sum += Math.pow(d, digits);
            num /= 10;
        }
        return sum == n;
    }

    public static void Armstrong(int s)
    {
        for (int i = 1; i <= s; i++)
        {
            if (isArmstrongNumber(i))
            {
                System.out.println(i);
            }
        }
    }
    public static void main(String[] args)
    {
        int k = 1000;
        Armstrong(k);
    }


}
